import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default class App extends React.Component {
	constructor() {
		super();

		this.state = {
			number: 1
		}
	}

	increase = () => {
		this.setState({
			number: this.state.number + 1
		})
	}

	decrease = () => {
		this.setState({
			number: this.state.number - 1
		})
	}
	
	render() {
		return (
			<View style={ styles.container }>
				<View style={ styles.header }>
					<Text style={ styles.headerText }>Contador</Text>
				</View>
				<View style={ styles.body }>
					<Text style={ styles.bodyText }> { this.state.number }</Text>
				</View>
				<View style={ styles.footer }>
					<Button style={ styles.footerButton1 } title="Aumentar" onPress={ () => { this.increase() } } />
					<Button title="Reducir" onPress={ () => { this.decrease() } } />
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	header: {
		flex: 1,
		backgroundColor: '#2980b9',
		alignItems: 'center',
		justifyContent: 'center',
	},
	headerText: {
		color: '#fff',
		fontSize: 25
	},
	body: {
		flex: 3,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	bodyText: {
		fontSize: 50
	},
	footer: {
		flex: 1,
		backgroundColor: '#ecf0f1',
		justifyContent: 'space-around',
	},
	footerButton1: {
		marginBottom: 10
	}
});
